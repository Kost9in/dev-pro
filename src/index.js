
/* less/css */
import 'font-awesome/less/font-awesome.less';
import './less/main.less';

/* packages */
import $ from 'jquery';

/* app */
import Print from './components/print';

$(() => {

  const DOM = {
    loader: '.loader',
    productList: '.choose-product',
    sidebar: '.sidebar',
    sidebarColor: '.sidebar .color-bg input[name="color"]',
    sidebarActions: '.sidebar .actions',
    sidebarLayers: '.sidebar .layers',
    content: '.content',
    worksheet: '.worksheet',
    worksheetColor: '.worksheet .color-bg',
    worksheetImage: '.worksheet .image-bg',
    worksheetOutline: '.worksheet .outline-bg',
    worksheetLayers: '.worksheet .layers',
    worksheetEdit: '.worksheet .edit',
    changeProduct: '.change-product',
    resetAll: '.reset-all',
    showResults: '.show-results',
    export: '.export',
    resultWrapper: '.popups .popup .canvas-wrapper',
    popupFade: '.popups .fade',
    popup: '.popups .popup',
    iconsList: '.popups .popup .add-icon',
    chooseImage: '.popups .popup .load-image',
    fileUploader: '.upload-image-input',
    imageForm: '.popups .popup .form-image',
    createImageLink: '.popups .popup .create-image',
    textForm: '.popups .popup .add-text-form',
    textFormText: '.popups .popup .add-text-form input[name="text"]',
    textFormFont: '.popups .popup .add-text-form select[name="font-size"]',
    textFormSize: '.popups .popup .add-text-form input[name="font-size"]',
    textFormColor: '.popups .popup .add-text-form input[name="color"]',
    textFormBold: '.popups .popup .add-text-form input[name="bold"]',
    textFormItalic: '.popups .popup .add-text-form input[name="italic"]',
    textFormCancel: '.popups .popup .add-text-form button.cancel',
    errorList: '.error-list',
    createImageCanvas: '.create-image-wrapper .canvas-create-wrapper canvas',
    createImageSizeValue: '.create-image-wrapper .tools-wrapper .size .value',
    createImageSizeInput: '.create-image-wrapper .tools-wrapper .size input',
    createImageOpacityValue: '.create-image-wrapper .tools-wrapper .opacity .value',
    createImageOpacityInput: '.create-image-wrapper .tools-wrapper .opacity input',
    createImageColor: '.create-image-wrapper .tools-wrapper .color input',
    createImageClearButton: '.create-image-wrapper .tools-wrapper .buttons .clear',
    createImageBackButton: '.create-image-wrapper .tools-wrapper .buttons .back',
    createImageSaveButton: '.create-image-wrapper .tools-wrapper .buttons .save'
  };

  const print = new Print(DOM);

});
