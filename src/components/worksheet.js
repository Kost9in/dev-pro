
import $ from 'jquery';
import config from '../config';
import products from '../data/products';

class Worksheet {

  constructor (config) {

    this.DOM = config.DOM;
    this.callbacks = config.callbacks;

    /* set active layer */
    this.DOM.layers.on('click', e => {
      const target = $(e.target).closest('a');
      if (target.attr('data-layer')) {
        e.preventDefault();
        const layer = parseInt(target.attr('data-layer'));
        this.callbacks.setActiveLayer(layer);
      }
    });

    /* remove active layer */
    this.DOM.worksheet.on('click', e => {
      if ($(e.target).is('canvas')) {
        this.callbacks.setActiveLayer(-1);
      }
    });

    /* remove drag event */
    this.DOM.layers.on('dragstart', e => e.preventDefault());


    /* drag */
    let edit = '';
    let coords = {};

    this.DOM.edit.on('mousedown', e => {
      const target = $(e.target).closest('a');
      if (target.length) {
         if (target.hasClass('resize')) {
          edit = 'resize';
          const buttonOffset = target.offset();
          coords = {
            buttonSizes: {
              width: target.width(),
              height: target.height()
            },
            cursorOnButton: {
              top: e.clientY - buttonOffset.top,
              left: e.clientX - buttonOffset.left
            },
            newSizes: {
              width: this.DOM.edit.width(),
              height: this.DOM.edit.height()
            }
          };
        } else {
          edit = '';
          coords = {};
        }
      } else {
        edit = 'drag';
        const editOffset = this.DOM.edit.offset();
        const editPosition = this.DOM.edit.position();
        coords = {
          cursorOnEdit: {
            top: e.clientY - editOffset.top,
            left: e.clientX - editOffset.left
          },
          newCoords: {
            top: editPosition.top,
            left: editPosition.left
          }
        };
      }
    });

    $('body').on('mousemove', e => {
      if (edit) {
        if (edit === 'drag') {
          const worksheetOffset = this.DOM.worksheet.offset();
          let top = e.clientY - worksheetOffset.top - coords.cursorOnEdit.top;
          const maxTop = this.DOM.worksheet.height() - this.DOM.edit.height() - 1;
          if (top < 1) {
            top = 1;
          }
          if (top > maxTop) {
            top = maxTop;
          }
          let left = e.clientX - worksheetOffset.left - coords.cursorOnEdit.left;
          const maxLeft = this.DOM.worksheet.width() - this.DOM.edit.width() - 1;
          if (left < 1) {
            left = 1;
          }
          if (left > maxLeft) {
            left = maxLeft;
          }
          coords.newCoords = { top, left };
          this.DOM.edit.css(coords.newCoords);
        } else if (edit === 'resize') {
          const worksheetOffset = this.DOM.worksheet.offset();
          const editPosition = this.DOM.edit.position();
          let width = e.clientX - worksheetOffset.left - editPosition.left - coords.cursorOnButton.left + coords.buttonSizes.width;
          const minWidth = 50;
          const maxWidth = this.DOM.worksheet.width() - editPosition.left - 1;
          if (width < minWidth) {
            width = minWidth;
          }
          if (width > maxWidth) {
            width = maxWidth;
          }
          let height = e.clientY - worksheetOffset.top - editPosition.top - coords.cursorOnButton.top + coords.buttonSizes.height;
          const minHeight = 50;
          const maxHeight = this.DOM.worksheet.height() - editPosition.top - 1;
          if (height < minHeight) {
            height = minHeight;
          }
          if (height > maxHeight) {
            height = maxHeight;
          }
          coords.newSizes = { width, height };
          this.DOM.edit.css(coords.newSizes);
        }
      }
    });

    $('body').on('mouseup', e => {
      if (edit) {
        if (edit === 'drag') {
          this.callbacks.setPosition(coords.newCoords);
        } else if (edit === 'resize') {
          this.callbacks.setSizes(coords.newSizes);
        }
        edit = '';
        coords = {};
      }
    });

    /* buttons drag */
    $(window).on('keydown', e => {
      if (e.keyCode >= 37 && e.keyCode <= 40) {
        const editPosition = this.DOM.edit.position();
        switch (e.keyCode) {
          case 37:
            editPosition.left--;
            break;
          case 38:
            editPosition.top--;
            break;
          case 39:
            editPosition.left++;
            break;
          case 40:
            editPosition.top++;
            break;
        }
        const maxTop = this.DOM.worksheet.height() - this.DOM.edit.height() - 1;
        if (editPosition.top < 1) {
          editPosition.top = 1;
        }
        if (editPosition.top > maxTop) {
          editPosition.top = maxTop;
        }
        const maxLeft = this.DOM.worksheet.width() - this.DOM.edit.width() - 1;
        if (editPosition.left < 1) {
          editPosition.left = 1;
        }
        if (editPosition.left > maxLeft) {
          editPosition.left = maxLeft;
        }
        this.DOM.edit.css(editPosition);
        this.callbacks.setPosition(editPosition);
      }
    });

    /* remove or edit */
    this.DOM.edit.on('click', e => {
      const target = $(e.target).closest('a');
      if (target.length) {
        if (target.hasClass('remove')) {
          this.callbacks.removeActiveLayer();
        } else if (target.hasClass('edit')) {
          this.callbacks.editActiveLayer();
        }
      }
    });

  }

  render (project) {
    if (project) {
      const product = products.find(product => product.id === project.product);
      if (product) {
        /* set worksheet */
        this.DOM.worksheet.width(product.worksheet.width);
        this.DOM.worksheet.height(product.worksheet.height);
        this.DOM.image.css({
          'background-image': `url(${product.src})`
        });
        /* set color canvas */
        const colorCanvas = document.createElement('canvas');
        colorCanvas.width = product.worksheet.width;
        colorCanvas.height = product.worksheet.height;
        const colorCtx = colorCanvas.getContext('2d');
        colorCtx.beginPath();
        product.colorPath.forEach((point, idx) => {
          if (idx === 0) {
            colorCtx.moveTo(point[0], point[1]);
          } else {
            colorCtx.lineTo(point[0], point[1]);
          }
        });
        colorCtx.lineTo(product.colorPath[0][0], product.colorPath[0][1]);
        colorCtx.fillStyle = project.color;
        colorCtx.fill();
        this.DOM.color.html('').append(colorCanvas);
        /* set outline canvas */
        const outlineCanvas = document.createElement('canvas');
        outlineCanvas.width = product.worksheet.width;
        outlineCanvas.height = product.worksheet.height;
        const outlineCtx = outlineCanvas.getContext('2d');
        outlineCtx.beginPath();
        product.outlinePath.forEach((point, idx) => {
          if (idx === 0) {
            outlineCtx.moveTo(point[0], point[1]);
          } else {
            outlineCtx.lineTo(point[0], point[1]);
          }
        });
        outlineCtx.lineTo(product.outlinePath[0][0], product.outlinePath[0][1]);
        outlineCtx.lineWidth = 1;
        outlineCtx.strokeStyle = '#FF0000';
        outlineCtx.setLineDash([4, 4]);
        outlineCtx.stroke();
        this.DOM.outline.html('').append(outlineCanvas);
        /* add layers */
        let layersHtml = '';
        project.layers.forEach((layer, idx) => {

          let layerHtml = '';
          if (idx !== project.activeLayer) {
            switch (layer.type) {
              case 'icon':
                layerHtml = `<img style="width: ${layer.width}px; height: ${layer.height}px" src="${layer.src}" alt="">`;
                break;
              case 'image':
                layerHtml = `<img style="width: ${layer.width}px; height: ${layer.height}px" src="${layer.base64}" alt="">`;
                break;
              case 'text':
                const style = `display: block;
                width: ${layer.width}px;
                height: ${layer.height}px;
                font-family: ${layer.font};
                font-size: ${layer.size}px;
                font-weight: ${layer.bold ? 'bold' : 'normal'};
                font-style: ${layer.italic ? 'italic' : 'normal'};
                line-height: ${config.lineHeight};
                color: ${layer.color};
                white-space: nowrap;`;
                layerHtml = `<p style="${style}">${layer.text}</p>`;
                break;
            }
            layersHtml += `<li style="top: ${layer.top}px; left: ${layer.left}px;"><a data-layer="${idx}">${layerHtml}</a></li>`;
          }
        });
        this.DOM.layers.html(`<ul>${layersHtml}</ul>`);
        /* add active layer */
        if (project.activeLayer !== -1 && project.layers[project.activeLayer]) {
          const activeLayer = project.layers[project.activeLayer];
          let editHtml = '';
          switch (activeLayer.type) {
            case 'icon':
              editHtml = `<img src="${activeLayer.src}" alt="">
                <a class="resize"><i class="fa fa-arrows-v"></i></a>
                <a class="remove"><i class="fa fa-trash"></i></a>`;
              break;
            case 'image':
              editHtml = `<img src="${activeLayer.base64}" alt="">
                <a class="resize"><i class="fa fa-arrows-v"></i></a>
                <a class="remove"><i class="fa fa-trash"></i></a>`;
              break;
            case 'text':
              const style = `display: block;
                font-family: ${activeLayer.font};
                font-size: ${activeLayer.size}px;
                font-weight: ${activeLayer.bold ? 'bold' : 'normal'};
                font-style: ${activeLayer.italic ? 'italic' : 'normal'};
                line-height: ${config.lineHeight};
                color: ${activeLayer.color};
                white-space: nowrap;`;
              editHtml = `<p style="${style}">${activeLayer.text}</p>
                <a class="edit"><i class="fa fa-pencil"></i></a>
                <a class="remove"><i class="fa fa-trash"></i></a>`;
              break;
          }
          this.DOM.edit.addClass(`show ${activeLayer.type}`).css({
            top: activeLayer.top,
            left: activeLayer.left,
            width: activeLayer.width,
            height: activeLayer.height
          }).html(editHtml);
        } else {
          this.DOM.edit.removeClass('show icon image text').html('');
        }
      }

    }
  }

};

export default Worksheet;
