
import $ from 'jquery';
import products from '../data/products';

class Sidebar {

  constructor (config) {

    this.DOM = config.DOM;
    this.callbacks = config.callbacks;

    /* color */
    config.DOM.color.on('change', e => {
      e.preventDefault();
      const color = $(e.currentTarget).val();
      config.callbacks.changeColor(color);
    });

    /* actions */
    config.DOM.actions.on('click', e => {
      const a = $(e.target).closest('a');
      if (a.attr('data-type')) {
        config.callbacks.add(a.attr('data-type'));
      }
    });

    /* layers */
    config.DOM.layers.on('click', e => {
      const a = $(e.target).closest('a');
      if (a.hasClass('remove')) {
        const layer = a.parents('li').attr('data-layer');
        config.callbacks.remove(layer);
      }
    });

    /* drag */
    let drag = -1;
    let coords = {};
    this.DOM.layers.on('mousedown', e => {
      if (!$(e.target).closest('a').length) {
        const target = $(e.target).closest('li');
        if (target.length && !target.hasClass('empty') && !target.hasClass('place')) {
          drag = target.attr('data-layer');
          const liPosition = target.position();
          const liOffset = target.offset();
          coords = {
            cursorOnLi: e.clientY - liOffset.top
          };
          target.addClass('drag').css({ top: liPosition.top }).before('<li class="place"></li>');
        } else {
          drag = -1;
          coords = {};
        }
      }
    });

    $('body').on('mousemove', e => {
      if (drag !== -1) {
        const drag = this.DOM.layers.find('li.drag');
        const topDrag = drag.position().top;
        const heightDrag = drag.height();
        const place = this.DOM.layers.find('li.place');
        const topPlace = place.position().top;
        const ul = this.DOM.layers.find('ul');
        const maxOffset = ul.height() - heightDrag;
        const layersOffset = ul.offset();
        let offset = e.clientY - layersOffset.top - coords.cursorOnLi;
        if (offset < 0) {
          offset = 0;
        }
        if (offset > maxOffset) {
          offset = maxOffset;
        }
        if (topDrag - topPlace + heightDrag < 0) {
          place.prev().before(place);
        }
        if (topDrag > topPlace + heightDrag) {
          place.next().after(place);
        }
        drag.css({ top: offset });
      }
    });

    $('body').on('mouseup', e => {
      if (drag !== -1) {
        this.DOM.layers.find('li.drag').remove();
        const newPlace = this.DOM.layers.find('li.place').index();
        config.callbacks.move(drag, newPlace);
        drag = -1;
        coords = {};
      }
    });

  }

  render (project) {
    if (project) {
      const product = products.find(product => product.id === project.product);
      if (product) {
        /* set color */
        this.DOM.color.val(project.color);
        /* render layers */
        let layersListHtml = '';
        if (project.layers.length) {
          layersListHtml = project.layers.reduce((list, layer, idx) => {
            let layerHtml = '';
            const cls = (project.activeLayer === idx) ? 'active' : '';
            switch (layer.type) {
              case 'icon':
                layerHtml = `<li class="${cls}" class="icon" data-layer="${idx}">
                              <div class="img"><img src="${layer.src}" alt=""></div>
                              <div class="desc">
                                <p class="type">Тип: Знак</p>
                                <p class="size">Размер: ${parseInt(layer.width)}x${parseInt(layer.height)}px</p>
                              </div>
                              <a class="remove"><i class="fa fa-trash"></i></a>
                            </li>`;
                break;
              case 'image':
                layerHtml = `<li class="${cls}" class="image" data-layer="${idx}">
                              <div class="img"><img src="${layer.base64}" alt=""></div>
                              <div class="desc">
                                <p class="type">Тип: Изображение</p>
                                <p class="size">Размер: ${parseInt(layer.width)}x${parseInt(layer.height)}px</p>
                              </div>
                              <a class="remove"><i class="fa fa-trash"></i></a>
                            </li>`;
                break;
              case 'text':
                layerHtml = `<li class="${cls}" class="text" data-layer="${idx}">
                              <div class="desc">
                                <p class="type">Тип: Текст</p>
                                <p style="color: ${layer.color};" class="text">${layer.text}</p>
                                <p class="font">${layer.font}, ${layer.size}px</p>
                              </div>
                              <a class="remove"><i class="fa fa-trash"></i></a>
                            </li>`;
                break;
            }
            return `${list}${layerHtml}`;
          }, '');
        } else {
          layersListHtml = '<li class="empty"><p>Вы не добавили ни одного слоя.</p></li>';
        }
        this.DOM.layers.find('ul').html(layersListHtml);
      }
    }
  }

}

export default Sidebar;
