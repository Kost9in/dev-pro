
import $ from 'jquery';
import Worksheet from './worksheet';
import Sidebar from './sidebar';
import Paint from './paint';
import config from '../config';
import products from '../data/products';
import icons from '../data/icons';
import fonts from '../data/fonts';

class Print {

  constructor (DOM) {

    /* DOM */
    this.DOM = {};
    for(const key in DOM) {
      this.DOM[key] = $(DOM[key]);
    }

    /* load project */
    this.project = this.loadProject();

    /* popups */
    this.DOM.popupFade.on('click', e => this.popup());
    this.DOM.popup.find('a.close').on('click', e => this.popup());

    /* sidebar ini */
    this.sidebarIni();
    this.DOM.sidebar.find('.open').on('click', this.sidebarIni.bind(this));

    /* change product */
    this.DOM.changeProduct.on('click', e => {
      e.preventDefault();
      this.resetProject.call(this);
    });

    /* reset all */
    this.DOM.resetAll.on('click', e => {
      e.preventDefault();
      this.resetProject.call(this, this.project.product);
    });

    /* show results */
    this.DOM.showResults.on('click', e => {
      e.preventDefault();
      this.renderResult();
    });

    this.DOM.export.on('click', e => {
      e.preventDefault();
      this.exportResult();
    });

    /* choose image */
    this.DOM.chooseImage.on('click', e => {
      e.preventDefault();
      this.DOM.fileUploader.trigger('click');
    });
    this.DOM.fileUploader.on('change', e => {
      const files = this.DOM.fileUploader.get(0).files;
      if (files && files[0]) {
        const file = files[0];
        if (file.type === 'image/jpeg' || file.type === 'image/png') {
          const fr = new FileReader();
          fr.onload = (e) => {
            this.addLayer('image', e.target.result);
          };
          fr.readAsDataURL(files[0]);
        } else {
          this.showError('Изображение должно быть в формате jpeg или png.');
        }
      }
    });

    /* add image as URL */
    this.DOM.imageForm.on('submit', e => {
      e.preventDefault();
      this.loader(true);
      const input = this.DOM.imageForm.find('input');
      const url = input.val();
      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = () => {
        if (xhr.response.type === 'image/jpeg' || xhr.response.type === 'image/png') {
          const fr = new FileReader();
          fr.onload = (e) => {
            input.val('');
            this.addLayer('image', e.target.result);
          };
          fr.readAsDataURL(xhr.response);
        } else {
          this.showError('Изображение должно быть в формате jpeg или png.');
        }
        this.loader(false);
      };
      xhr.onerror = (e) => {
        this.showError('Произошла ошибка при загрузке изображения.');
        this.loader(false);
      };
      xhr.open('GET', url);
      xhr.send();
    });

    /* create image popup */
    this.DOM.createImageLink.on('click', e => {
      e.preventDefault();
      this.popup('create-image');
    });

    /* error list */
    this.DOM.errorList.on('click', e => {
      const target = $(e.target).closest('li');
      if (target.length) {
        e.preventDefault();
        target.slideUp(300, () => target.remove());
      }
    });

    /* text form */
    this.DOM.textForm.on('submit', e => {
      e.preventDefault();
      const text = this.DOM.textFormText.val().trim();
      const font = this.DOM.textFormFont.val();
      const size = this.DOM.textFormSize.val();
      const color = this.DOM.textFormColor.val();
      const bold = this.DOM.textFormBold.prop('checked');
      const italic = this.DOM.textFormItalic.prop('checked');
      if (text === '') {
        this.showError('Поле "Текст" не должно быть пустым.');
      } else if (size < config.fontSize.min || size > config.fontSize.max) {
        this.showError(`Размер шрифта должен быть в диапазоне от ${config.fontSize.min} до ${config.fontSize.max} px.`);
      } else {
        let layerId = parseInt(this.DOM.textForm.attr('data-layer'));
        if (layerId !== -1) {
          const layer = this.project.layers[layerId];
          if (!layer || layer.type !== 'text') {
            layerId = -1;
          }
        }
        if (layerId === -1) {
          const newLayer = this.cropTextAndGetPosition({ text, font, size, color, bold, italic });
          Object.assign(newLayer, { font, size, color });
          this.addLayer('text', newLayer);
        } else {
          const newLayer = this.cropTextAndGetPosition({ text, font, size, color, bold, italic }, this.project.layers[layerId]);
          this.project.layers[layerId] = newLayer;
          this.saveProject(this.project);
          this.popup();
        }
      }
    });
    this.DOM.textFormCancel.on('click', e => {
      e.preventDefault();
      this.popup();
    });

    /* render */
    this.worksheet = new Worksheet({
      DOM: {
        worksheet: this.DOM.worksheet,
        color: this.DOM.worksheetColor,
        image: this.DOM.worksheetImage,
        outline: this.DOM.worksheetOutline,
        layers: this.DOM.worksheetLayers,
        edit: this.DOM.worksheetEdit
      },
      callbacks: {
        setActiveLayer: idx => {
          this.project.activeLayer = idx;
          this.saveProject(this.project);
        },
        removeActiveLayer: () => {
          this.project.layers.splice(this.project.activeLayer, 1);
          this.project.activeLayer = -1;
          this.saveProject(this.project);
        },
        editActiveLayer: () => {
          const activeLayer = this.project.layers[this.project.activeLayer];
          this.DOM.textForm.attr('data-layer', this.project.activeLayer);
          this.DOM.textFormText.val(activeLayer.text);
          this.DOM.textFormFont.val(activeLayer.font);
          this.DOM.textFormSize.val(activeLayer.size);
          this.DOM.textFormColor.val(activeLayer.color);
          this.DOM.textFormBold.prop('checked', activeLayer.bold);
          this.DOM.textFormItalic.prop('checked', activeLayer.italic);
          this.popup('text');
        },
        setPosition: (coords) => {
          this.project.layers[this.project.activeLayer].top = coords.top;
          this.project.layers[this.project.activeLayer].left = coords.left;
          this.saveProject(this.project);
        },
        setSizes: (sizes) => {
          this.project.layers[this.project.activeLayer].width = sizes.width;
          this.project.layers[this.project.activeLayer].height = sizes.height;
          this.saveProject(this.project);
        }
      }
    });

    this.sidebar = new Sidebar({
      DOM: {
        sidebar: this.DOM.sidebar,
        color: this.DOM.sidebarColor,
        actions: this.DOM.sidebarActions,
        layers: this.DOM.sidebarLayers
      },
      callbacks: {
        changeColor: color => {
          this.project.color = color;
          this.saveProject(this.project);
        },
        add: type => {
          switch (type) {
            case 'icon':
              this.popup('icons');
              break;
            case 'image':
              this.popup('image');
              break;
            case 'text':
              this.DOM.textForm.attr('data-layer', -1);
              this.DOM.textFormText.val('');
              this.DOM.textFormFont.val(config.defaultFont);
              this.DOM.textFormSize.val(config.fontSize.default);
              this.DOM.textFormColor.val(config.defaultFontColor);
              this.DOM.textFormBold.prop('checked', false);
              this.DOM.textFormItalic.prop('checked', false);
              this.popup('text');
              break;
          }
        },
        remove: layerId => {
          this.project.layers.splice(layerId, 1);
          this.project.activeLayer = -1;
          this.saveProject(this.project);
        },
        move: (oldIdx, newIdx) => {
          const movedLayer = this.project.layers.splice(oldIdx, 1);
          this.project.layers.splice(newIdx, 0, movedLayer[0]);
          this.project.activeLayer = newIdx;
          this.saveProject(this.project);
        }
      }
    });

    this.paint = new Paint({
      DOM: {
        canvas: this.DOM.createImageCanvas,
        sizeValue: this.DOM.createImageSizeValue,
        sizeInput: this.DOM.createImageSizeInput,
        opacityValue: this.DOM.createImageOpacityValue,
        opacityInput: this.DOM.createImageOpacityInput,
        color: this.DOM.createImageColor,
        clearButton: this.DOM.createImageClearButton,
        backButton: this.DOM.createImageBackButton,
        saveButton: this.DOM.createImageSaveButton
      },
      callbacks: {
        back: () => {
          this.popup();
        },
        save: image => {
          this.addLayer('image', image);
        }
      }
    });

    this.renderProductList(products);
    this.renderIconsList(icons);
    this.renderFonts(fonts);
    this.render();
    this.loader(false);

  }

  /* project */

  loadProject () {
    const newProject = {
      product: 0,
      activeLayer: -1,
      color: config.defaultBgColor,
      layers: []
    };
    return (localStorage.project) ? JSON.parse(localStorage.project) : newProject;
  }

  saveProject (project) {
    localStorage.project = JSON.stringify(project);
    this.render();
  }

  resetProject (productId) {
    localStorage.project = '';
    this.project = this.loadProject();
    if (productId) {
      this.project.product = productId;
    }
    this.saveProject(this.project);
  }

  setProduct (productId) {
    if (products.find(product => product.id === productId)) {
      this.project.product = productId;
      this.saveProject(this.project);
    } else {
      this.resetProject();
    }
  }

  sidebarIni (e) {
    let status = localStorage.sidebar === 'true';
    if (e) {
      e.preventDefault();
      status = !status;
    }
    if (status) {
      this.DOM.sidebar.addClass('open');
      this.DOM.content.addClass('open');
    } else {
      this.DOM.sidebar.removeClass('open');
      this.DOM.content.removeClass('open');
    }
    localStorage.sidebar = status;
  }

  cropTextAndGetPosition (config, layer) {
    const worksheet = products.find(product => product.id === this.project.product).worksheet;
    let text = config.text;
    const tmp = $(document.createElement('p')).text(config.text).css({
      'display': 'inline-block',
      'font-family': config.font,
      'font-size': `${config.size}px`,
      'font-weight': config.bold ? 'bold' : 'normal',
      'font-style': config.italic ? 'italic' : 'normal',
      'white-space': 'nowrap'
    });
    $('body').append(tmp);
    while (tmp.width() > worksheet.width && text.length) {
      text = text.slice(0, -1);
      tmp.text(text);
    }
    const width = tmp.width();
    const height = tmp.height();
    tmp.remove();
    if (layer === undefined) {
      const top = (worksheet.height - height) / 2;
      const left = (worksheet.width - width) / 2;
      return { text, width, height, top, left };
    } else {
      Object.assign(layer, config, { text, width, height });
      if (layer.left + width > worksheet.width) {
        layer.left = worksheet.width - width - 1;
      }
      if (layer.top + height > worksheet.height) {
        layer.top = worksheet.height - height - 1;
      }
      return layer;
    }
  }

  addLayer (type, data) {
    if (this.project.layers.length < config.maxLayerCount) {
      const getNewLayer = new Promise(resolve => {
        const newLayer = { type };
        const getImagePosition = (image) => {
          const worksheet = products.find(product => product.id === this.project.product).worksheet;
          let scale = 1;
          const widthScale = worksheet.width * .9 / image.width;
          const heightScale = worksheet.height * .9 / image.height;
          if (widthScale < 1 && widthScale <= heightScale) {
            scale = widthScale;
          }
          if (heightScale < 1 && heightScale <= widthScale) {
            scale = heightScale;
          }
          const width = image.width * scale;
          const height = image.height * scale;
          const top = (worksheet.height - height) / 2;
          const left = (worksheet.width - width) / 2;
          return { width, height, top, left };
        };
        switch (type) {
          case 'icon':
            const iconImage = new Image();
            iconImage.src = data.src;
            iconImage.onload = () => {
              const position = getImagePosition(iconImage);
              Object.assign(newLayer, position, { src: data.src });
              resolve(newLayer);
            };
            break;
          case 'image':
            const image = new Image();
            image.src = data;
            image.onload = () => {
              const position = getImagePosition(image);
              Object.assign(newLayer, position, { base64: data });
              resolve(newLayer);
            };
            break;
          case 'text':
            Object.assign(newLayer, data);
            resolve(newLayer);
            break;
        }
      });
      getNewLayer.then((newLayer) => {
        this.popup();
        this.project.layers.push(newLayer);
        this.project.activeLayer = this.project.layers.length - 1;
        this.saveProject(this.project);
      });
    } else {
      this.showError('У Вас добавлено максимальное кол-во слоев.');
      this.popup();
    }
  }

  /* render */

  popup (popup) {
    let popupObject = null;
    if (popup) {
      popupObject = this.DOM.popup.filter(`[data-popup="${popup}"]`);
    }
    if (popupObject) {
      this.DOM.popupFade.addClass('open');
      popupObject.addClass('open');
      setTimeout(() => this.DOM.popup.filter(`:not([data-popup="${popup}"])`).removeClass('open'), 300);
    } else {
      this.DOM.popupFade.removeClass('open');
      this.DOM.popup.removeClass('open');
    }
  }

  showError (error) {
    const li = $(document.createElement('li'));
    li.css('display', 'none').html(`<p>${error}</p>`);
    this.DOM.errorList.find('ul').append(li);
    li.fadeIn(300);
    setTimeout(() => li.slideUp(300, () => li.remove()), 3000);
  }

  loader (status) {
    if (status) {
      this.DOM.loader.removeClass('hide');
    } else {
      this.DOM.loader.addClass('hide');
    }
  }

  renderProductList (products) {
    const list = products.reduce((list, product) =>
      `${list}<li><a data-product="${product.id}"><img src="${product.src}" alt=""><p>${product.title}</p></a></li>`, '');
    this.DOM.productList.find('ul').html(list).off('click').on('click', e => {
      const target = $(e.target).closest('a');
      if (target.attr('data-product')) {
        e.preventDefault();
        const product = parseInt(target.attr('data-product'));
        this.setProduct(product);
      }
    });
  }

  renderIconsList (icons) {
    const list = icons.reduce((list, icon) =>
      `${list}<li><a data-icon="${icon.id}"><img src="${icon.src}" alt=""><p>${icon.title}</p></a></li>`, '');
    this.DOM.iconsList.html(list).off('click').on('click', e => {
      const target = $(e.target).closest('a');
      if (target.attr('data-icon')) {
        e.preventDefault();
        const iconId = parseInt(target.attr('data-icon'));
        this.addLayer('icon', icons.find(icon => icon.id === iconId));
      }
    });
  }

  renderFonts (fonts) {
    const list = fonts.reduce((list, font) =>
      `${list}<option style="font-family: '${font.key}'" value="${font.key}">${font.title}</option>`, '');
    fonts.forEach(font => $('head').append(`<link href="${font.link}" rel="stylesheet"> `));
    this.DOM.textFormFont.html(list);
  }

  render () {
    /* choose product */
    if (this.project.product) {
      this.DOM.productList.removeClass('show');
    } else {
      this.DOM.productList.addClass('show');
    }
    this.worksheet.render(this.project);
    this.sidebar.render(this.project);
  }

  resultToCanvas () {

    const activeProduct = products.find(product => product.id === this.project.product);
    const canvas = document.createElement('canvas');
    canvas.width = activeProduct.worksheet.width;
    canvas.height = activeProduct.worksheet.height;
    const ctx = canvas.getContext('2d');

    /* add color bg */
    ctx.beginPath();
    activeProduct.colorPath.forEach((point, idx) => {
      if (idx === 0) {
        ctx.moveTo(point[0], point[1]);
      } else {
        ctx.lineTo(point[0], point[1]);
      }
    });
    ctx.fillStyle = this.project.color;
    ctx.fill();

    /* load bg image */
    const loadBg = new Promise(resolve => {
      const bg = new Image();
      bg.src = activeProduct.src;
      bg.onload = () => {
        resolve(bg);
      };
    });

    const drawLayer = (layer) => {
        const loadLayer = new Promise(resolve => {
          switch (layer.type) {
            case 'icon':
              const iconImage = new Image();
              iconImage.src = layer.src;
              iconImage.onload = () => {
                ctx.drawImage(iconImage, layer.left, layer.top, layer.width, layer.height);
                resolve();
              };
              break;
            case 'image':
              const image = new Image();
              image.src = layer.base64;
              image.onload = () => {
                ctx.drawImage(image, layer.left, layer.top, layer.width, layer.height);
                resolve();
              };
              break;
            case 'text':
              ctx.fillStyle = layer.color;
              ctx.textBaseline = 'top';
              ctx.font = `${layer.bold ? 'bold' : ''} ${layer.italic ? 'italic' : ''} ${layer.size}px ${layer.font}`;
              ctx.fillText(layer.text, layer.left, layer.top);
              resolve();
              break;
          }
        });
        return loadLayer;
    };

    return new Promise((resolve) => {
      loadBg.then((bg) => {
        ctx.drawImage(bg, 0, 0, activeProduct.worksheet.width, activeProduct.worksheet.height);
        /* clip */
        ctx.beginPath();
        activeProduct.outlinePath.forEach((point, idx) => {
          if (idx === 0) {
            ctx.moveTo(point[0], point[1]);
          } else {
            ctx.lineTo(point[0], point[1]);
          }
        });
        ctx.clip();
        /* add layers */
        const layers = Object.assign([], this.project.layers);
        layers.reduce((p, layer) => p.then(() => drawLayer(layer)), Promise.resolve()).then(() => {
          resolve(canvas);
        });
      });
    });

  }

  renderResult () {
    this.loader(true);
    this.resultToCanvas().then((canvas) => {
      this.DOM.resultWrapper.html('').append(canvas);
      this.popup('result');
      this.loader(false);
    });
  }

  exportResult () {
    this.loader(true);
    this.resultToCanvas().then((canvas) => {
      window.open(canvas.toDataURL(),'_blank');
      this.loader(false);
    });
  }

}

export default Print;
