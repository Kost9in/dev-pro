
import $ from 'jquery';

class Paint {

  constructor (config) {

    this.DOM = config.DOM;
    this.callbacks = config.callbacks;
    this.canvas = this.DOM.canvas.get(0);
    this.canvas.width = 400;
    this.canvas.height = 400;
    this.context = this.canvas.getContext('2d');

    /* size */
    this.DOM.sizeInput.on('change', e => {
      e.preventDefault();
      const size = $(e.currentTarget).val();
      this.setSize(size);
    });

    /* opacity */
    this.DOM.opacityInput.on('change', e => {
      e.preventDefault();
      const opacity = $(e.currentTarget).val();
      this.setOpacity(opacity);
    });

    /* color */
    this.DOM.color.on('change', e => {
      e.preventDefault();
      const color = $(e.currentTarget).val();
      this.setColor(color);
    });

    /* clear */
    this.DOM.clearButton.on('click', e => {
      e.preventDefault();
      this.clear();
    });

    /* back */
    this.DOM.backButton.on('click', e => {
      e.preventDefault();
      this.callbacks.back();
    });

    /* save */
    this.DOM.saveButton.on('click', e => {
      e.preventDefault();
      const image = this.canvas.toDataURL();
      this.clear();
      this.callbacks.save(image);
    });

    /* draw */
    let draw = false;

    this.DOM.canvas.on('mousedown', e => {
      draw = true;
      const canvasOffset = this.DOM.canvas.offset();
      const x = e.clientX - canvasOffset.left;
      const y = e.clientY - canvasOffset.top;
      this.draw(x, y);
    });

    $('body').on('mousemove', e => {
      if (draw) {
        const canvasOffset = this.DOM.canvas.offset();
        const canvasWidth = this.DOM.canvas.width();
        const canvasHeight = this.DOM.canvas.height();
        const x = e.clientX - canvasOffset.left;
        const y = e.clientY - canvasOffset.top;
        if (x >= 0 && x <= canvasWidth && y >= 0 && y <= canvasHeight) {
          this.draw(x, y);
        }
      }
    });

    $('body').on('mouseup', e => {
      draw = false;
    });

    this.setDefaultValues();
  }

  draw (x, y) {
    this.context.fillStyle = this.color;
    this.context.globalAlpha = this.opacity;
    this.context.beginPath();
    this.context.arc(x, y, this.size / 2, 0, 2 * Math.PI, false);
    this.context.fill();
    this.context.closePath();
  }

  clear () {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  setSize (size) {
    this.size = size;
    this.DOM.sizeValue.text(size);
    this.DOM.sizeInput.val(size);
  }

  setOpacity (opacity) {
    this.opacity = opacity;
    this.DOM.opacityValue.text(opacity);
    this.DOM.opacityInput.val(opacity);
  }

  setColor (color) {
    this.color = color;
    this.DOM.color.val(color);
  }

  setDefaultValues () {
    this.setSize(10);
    this.setOpacity(100);
    this.setColor('#000000');
  }

}

export default Paint;
