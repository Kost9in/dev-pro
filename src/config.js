
export default {
  maxLayerCount: 5,
  defaultBgColor: '#ffffff',
  defaultFontColor: '#000000',
  defaultFont: 'Lobster',
  fontSize: {
    min: 18,
    max: 96,
    default: 48
  },
  lineHeight: '91%'
};
