
const icons = [
  {
    id: 1,
    src: 'images/icon-1.png',
    title: 'Webpack'
  },
  {
    id: 2,
    src: 'images/icon-2.png',
    title: 'Angular'
  },
  {
    id: 3,
    src: 'images/icon-3.png',
    title: 'Bower'
  },
  {
    id: 4,
    src: 'images/icon-4.png',
    title: 'Grunt'
  },
  {
    id: 5,
    src: 'images/icon-5.png',
    title: 'NPM'
  },
  {
    id: 6,
    src: 'images/icon-6.png',
    title: 'LESS'
  },
  {
    id: 7,
    src: 'images/icon-7.png',
    title: 'React'
  },
  {
    id: 8,
    src: 'images/icon-8.png',
    title: 'Sublime'
  },
  {
    id: 9,
    src: 'images/icon-9.png',
    title: 'Gulp'
  },
  {
    id: 10,
    src: 'images/icon-10.png',
    title: 'DevChallenge'
  }
];

export default icons;
