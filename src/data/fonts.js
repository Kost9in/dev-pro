
const fonts = [
  {
    key: 'Open Sans Condensed',
    title: 'Open Sans Condensed',
    link: 'https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300'
  },
  {
    key: 'PT Sans Narrow',
    title: 'PT Sans Narrow',
    link: 'https://fonts.googleapis.com/css?family=PT+Sans+Narrow'
  },
  {
    key: 'Lobster',
    title: 'Lobster',
    link: 'https://fonts.googleapis.com/css?family=Lobster'
  },
  {
    key: 'Ubuntu Condensed',
    title: 'Ubuntu Condensed',
    link: 'https://fonts.googleapis.com/css?family=Ubuntu+Condensed'
  },
  {
    key: 'Play',
    title: 'Play',
    link: 'https://fonts.googleapis.com/css?family=Play'
  },
  {
    key: 'Poiret One',
    title: 'Poiret One',
    link: 'https://fonts.googleapis.com/css?family=Poiret+One'
  },
  {
    key: 'Comfortaa',
    title: 'Comfortaa',
    link: 'https://fonts.googleapis.com/css?family=Comfortaa'
  },
  {
    key: 'Bad Script',
    title: 'Bad Script',
    link: 'https://fonts.googleapis.com/css?family=Bad+Script'
  },
  {
    key: 'Neucha',
    title: 'Neucha',
    link: 'https://fonts.googleapis.com/css?family=Neucha'
  },
  {
    key: 'Ruslan Display',
    title: 'Ruslan Display',
    link: 'https://fonts.googleapis.com/css?family=Ruslan+Display'
  }
];

export default fonts;
